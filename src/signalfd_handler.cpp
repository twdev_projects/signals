#include "signals/signalfd_handler.h"

#include <sys/epoll.h>
#include <sys/signalfd.h>
#include <unistd.h>

#include <cerrno>
#include <cstring>
#include <stdexcept>

namespace signals
{
    SignalFdHandler::SignalFdHandler(const ::sigset_t& sigSet,
            std::unique_ptr<SignalHandlerCallbacks> cb,
            std::shared_ptr<FdOps> ops) :
        SignalHandler(std::move(cb)),
        eventFd{ops},
        signalFd{sigSet, ops},
        epollFd{ops}
    {
        epollFd.addEventSource(eventFd);
        epollFd.addEventSource(signalFd);
    }

    void SignalFdHandler::stopImpl() {
        eventFd.notify();
    }

    bool SignalFdHandler::runImpl() {
        for (const auto& fd : epollFd.wait()) {

            if (fd == eventFd.get()) {
                eventFd.wait();
                return true;
            } else if (fd == signalFd.get()) {
                const auto sigInfo = signalFd.wait();
                if (callbacks->onSignal(sigInfo)) {
                    stop();
                }
            } else {
                throw std::runtime_error("activity on unknown file descriptor");
            }
        }

        return false;
    }
} /* namespace signals */
