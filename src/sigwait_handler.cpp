#include "signals/sigwait_handler.h"
#include "signals/sigset_util.h"

#include <cerrno>
#include <string>
#include <cstring>
#include <stdexcept>

namespace
{
    ::sigset_t createSet(::sigset_t set) {
        signals::extendSet(set, SIGUSR1);
        return set;
    }

} /* namespace */

namespace signals
{
    SigWaitHandler::SigWaitHandler(const ::sigset_t& set,
            std::unique_ptr<SignalHandlerCallbacks> cb) :
        SignalHandler(std::move(cb)),
        waitSet{::createSet(set)},
        pid{::getpid()}
    {
    }

    void SigWaitHandler::stopImpl() {
        ::kill(pid, SIGUSR1);
    }

    bool SigWaitHandler::runImpl() {
        siginfo_t siginfo;
        const int sigNo = ::sigwaitinfo(&waitSet, &siginfo);
        const auto err = errno;

        if (sigNo == -1 && err != EAGAIN) {
            const std::string errStr = ::strerror(errno);
            throw std::runtime_error(errStr);
        } else if (sigNo > 0) {
            if (!isRunning.load() && sigNo == SIGUSR1 && siginfo.si_pid == pid) {
                // stop has been explicitly requested
                return true;
            } else {
                // call user's signal handler, if the signal handler
                // returns true, interpret that as an request to stop
                // handling signals
                const auto& si = SigInfo::fromSigInfo(siginfo);
                if (callbacks->onSignal(si)) {
                    stop();
                }
            }
        }

        return false;
    }

} /* namespace signals */
