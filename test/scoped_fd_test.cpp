#include <gmock/gmock.h>

#include "signals/scoped_fd.h"

#include <fcntl.h>
#include <cerrno>

using namespace ::testing;

class MockFdOps : public signals::FdOps {
public:
    MOCK_METHOD1(close, void(int));
};

class ScopedFdTest : public ::testing::Test {
public:
    void SetUp() {
        fdOps = std::make_shared<MockFdOps>();
    }

    void TearDown() {
        fdOps.reset();
    }

protected:
    std::shared_ptr<MockFdOps> fdOps;
};

TEST_F(ScopedFdTest, testClosing) {
    EXPECT_CALL(*fdOps, close(123));
    signals::ScopedFd fd1(123, fdOps);
}

TEST_F(ScopedFdTest, testMoving) {
    EXPECT_CALL(*fdOps, close(123));
    EXPECT_CALL(*fdOps, close(456));

    signals::ScopedFd fd1(123, fdOps);
    fd1 = signals::ScopedFd(456, fdOps);
}

TEST_F(ScopedFdTest, testClosingWithInvalidFd) {
    EXPECT_CALL(*fdOps, close(_)).Times(0);
    signals::ScopedFd fd1(-1, fdOps);
}

TEST_F(ScopedFdTest, testClosingAfterMoveWithInvalidFd) {
    EXPECT_CALL(*fdOps, close(_)).Times(0);
    EXPECT_CALL(*fdOps, close(456)).Times(1);

    signals::ScopedFd fd1(-1, fdOps);
    fd1 = signals::ScopedFd(456, fdOps);
}
