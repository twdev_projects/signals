#ifndef SIGINFO_H_
#define SIGINFO_H_

#include <signal.h>
#include <sys/signalfd.h>

namespace signals {

struct SigInfo {
  int sigNo;
  ::pid_t senderPid;
  int sentInt;
  void *sentPtr;

  static SigInfo fromSigInfo(const ::signalfd_siginfo &si);

  static SigInfo fromSigInfo(const ::siginfo_t &si);
};

} // namespace signals

#endif // SIGINFO_H_
