#ifndef SIGNAL_HANDLER_H_
#define SIGNAL_HANDLER_H_

#include <signals/symbols.h>
#include <signals/signal_handler_callbacks.h>

#include <atomic>
#include <memory>

namespace signals
{
    class SYMEXPORT SignalHandler {
    public:
        virtual ~SignalHandler();

        SignalHandler(std::unique_ptr<SignalHandlerCallbacks> cb);

        virtual void stop();

        virtual void run();

    protected:
        std::unique_ptr<SignalHandlerCallbacks> callbacks;
        std::atomic<bool> isRunning{false};

    private:
        virtual void stopImpl() = 0;

        virtual bool runImpl() = 0;
    };

} /* namespace signals */


#endif /* SIGNAL_HANDLER_H_ */
