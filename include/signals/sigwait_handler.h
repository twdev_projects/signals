#ifndef SIGWAIT_HANDLER_H_
#define SIGWAIT_HANDLER_H_

#include "signals/symbols.h"
#include "signals/signal_handler.h"
#include "signals/signal_handler_callbacks.h"

#include <signal.h>

#include <atomic>
#include <memory>

namespace signals
{
    class SYMEXPORT SigWaitHandler :
        public SignalHandler
    {
    public:
        SigWaitHandler(const ::sigset_t& set,
                std::unique_ptr<SignalHandlerCallbacks> cb);

    private:
        const ::sigset_t waitSet;
        const ::pid_t pid;
        std::atomic<bool> isRunning;

        void stopImpl();
        bool runImpl();
    };

} /* namespace signals */

#endif /* SIGWAIT_HANDLER_H_ */
