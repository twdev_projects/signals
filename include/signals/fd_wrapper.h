#ifndef FD_WRAPPER_H_
#define FD_WRAPPER_H_

#include "signals/scoped_fd.h"
#include "signals/symbols.h"

#include <string>

namespace signals
{
    class SYMEXPORT FdWrapper {
    public:
        virtual ~FdWrapper() = default;

        FdWrapper(int fd, const std::string& name, std::shared_ptr<FdOps> ops);

        int get() const noexcept;

    protected:
        ScopedFd fd;
        const std::string name;
    };

} /* namespace signals */


#endif /* FD_WRAPPER_H_ */
