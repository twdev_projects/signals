#ifndef SIGSET_UTIL_H_
#define SIGSET_UTIL_H_

#include "signals/symbols.h"

#include <signal.h>

#include <utility>

namespace signals
{
    template <typename ... SigT>
        void extendSet(::sigset_t& sigSet, SigT... sigs) {
            (sigaddset(&sigSet, sigs), ... );
        }

    template <typename ... SigT>
        ::sigset_t createSet(SigT... sigs) {
            ::sigset_t set;
            ::sigemptyset(&set);
            extendSet(set, std::forward<SigT>(sigs)...);
            return set;
        }

    ::sigset_t createFullSet() noexcept SYMEXPORT;
} /* namespace signals */

#endif /* SIGSET_UTIL_H_ */
