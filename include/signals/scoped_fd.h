#ifndef SCOPED_FD_H_
#define SCOPED_FD_H_

#include "signals/symbols.h"

#include <memory>

namespace signals
{
    class SYMEXPORT FdOps {
    public:
        virtual ~FdOps() = default;

        virtual void close(int fd) = 0;
    };

    class SYMEXPORT RealFdOps : public FdOps {
    public:
        void close(int fd) override;
    };

    class SYMEXPORT ScopedFd {
        constexpr static int InvalidFd = -1;
    public:
        ~ScopedFd();

        explicit ScopedFd(int fd, std::shared_ptr<FdOps> ops);

        ScopedFd(ScopedFd&& o);

        ScopedFd& operator=(ScopedFd&& o);


        ScopedFd(const ScopedFd&) = delete;
        ScopedFd& operator=(const ScopedFd&) = delete;

        int get() const noexcept;

        bool isValid() const noexcept;

    private:
        int fd;
        std::shared_ptr<FdOps> ops;
    };

} /* namespace signals */

#endif /* SCOPED_FD_H_ */
