#ifndef SCOPED_SIGSET_H_
#define SCOPED_SIGSET_H_

#include <signals/symbols.h>

#include <signal.h>

namespace signals
{
    class SYMEXPORT ScopedSigSet {
    public:
        ~ScopedSigSet();
        explicit ScopedSigSet(const ::sigset_t& sigSet);

    private:
        const ::sigset_t oldSet;
    };
} /* namespace signals */



#endif /* SCOPED_SIGSET_H_ */
