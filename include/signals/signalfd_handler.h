#ifndef SIGNALFD_HANDLER_H_
#define SIGNALFD_HANDLER_H_

#include "signals/fd_wrappers.h"
#include "signals/signal_handler.h"
#include "signals/signal_handler_callbacks.h"
#include "signals/symbols.h"

#include <signal.h>

namespace signals
{
    class SYMEXPORT SignalFdHandler :
        public SignalHandler
    {
    public:
        SignalFdHandler(const ::sigset_t& sigSet,
                std::unique_ptr<SignalHandlerCallbacks> cb,
                std::shared_ptr<FdOps> ops);

        void stopImpl();
        bool runImpl();

    private:
        EventFd eventFd;
        SignalFd signalFd;
        EpollFd epollFd;
    };
} /* namespace signals */

#endif /* SIGNALFD_HANDLER_H_ */
