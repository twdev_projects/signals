#ifndef FACTORY_H_
#define FACTORY_H_

#include "signals/signal_handler.h"
#include "signals/scoped_fd.h"
#include "signals/symbols.h"

#include <signal.h>

namespace signals
{
    class SYMEXPORT SignalHandlerFactory {
    public:
        SignalHandlerFactory();

        std::unique_ptr<SignalHandler> createSignalFdHandler(
                const ::sigset_t& sigSet,
                std::unique_ptr<SignalHandlerCallbacks> cb);

        std::unique_ptr<SignalHandler> createSigWaitHandler(
                const ::sigset_t& sigSet,
                std::unique_ptr<SignalHandlerCallbacks> cb);

    private:
        std::shared_ptr<FdOps> ops;
    };

} /* namespace signals */


#endif /* FACTORY_H_ */
