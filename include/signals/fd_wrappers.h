#ifndef FD_WRAPPERS_H_
#define FD_WRAPPERS_H_

#include "signals/fd_wrapper.h"
#include "signals/signal_handler_callbacks.h"
#include "signals/symbols.h"

#include <vector>

namespace signals
{
    class SYMEXPORT SignalFd :
        public FdWrapper {
    public:
        SignalFd(const ::sigset_t& sigSet, std::shared_ptr<FdOps> ops);

        SigInfo wait();
    };

    class SYMEXPORT EventFd :
        public FdWrapper {
    public:
        EventFd(std::shared_ptr<FdOps> ops);

        void notify() noexcept;

        void wait();
    };

    class SYMEXPORT EpollFd :
        public FdWrapper {
    public:

        EpollFd(std::shared_ptr<FdOps> ops);

        void addEventSource(const FdWrapper& fdw);

        std::vector<int> wait();
    };

} /* namespace signals */

#endif /* FD_WRAPPERS_H_ */
