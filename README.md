# libsignals

This library implements an object oriented signal handler for Linux systems.  It provides two implementations at the moment:

- `SigWaitHandler`
- `SignalFdHandler`

As the names imply, the former one is based on `sigwaitinfo` the latter employs
`signalfd`.  Both of them follow the interface defined in `SignalHandler`.

## Usage

Below is a fully functional example with a signal handler running on a dedicated thread:

```C++
#include <signals/sigset_util.h>
#include <signals/scoped_sigset.h>
#include <signals/factory.h>

#include <iostream>
#include <memory>
#include <thread>

class MyCallbacks :
    public signals::SignalHandlerCallbacks
{
public:
    // this callback will be executed on the same thread that runs SignalHandler::run() function
    bool onSignal(int sigNo, ::pid_t pid) {
        std::cout << "received signal: " << sigNo << ", from: " << pid << std::endl;
        return (sigNo == SIGTERM);
    }
};

int main(int argc, const char *argv[])
{
    // block signal reception prior to any children threads creation
    signals::ScopedSigSet sss{signals::createFullSet()};

    // define a signal set that the signal handler will service
    const auto sigSet =
        signals::createSet(SIGUSR1, SIGUSR2, SIGTERM, SIGINT);

    signals::SignalHandlerFactory factory{};

    // instantiate the signal handler (you can choose between implementations)
    // auto handler = factory.createSigWaitHandler(sigSet, std::make_unique<MyCallbacks>());
    auto handler = factory.createSignalFdHandler(sigSet, std::make_unique<MyCallbacks>());

    // run it
    std::thread t{[&](){
        handler->run();
    }};

    // do anything else
    std::cout << "in main thread" << std::endl;

    t.join();
    return 0;
}
```

For more information, please refer to a [blogpost](https://twdev.blog/2021/07/signals/).
