project('signals', 'cpp',
  version : '0.1',
  default_options : ['warning_level=3', 'cpp_std=c++17'])

# These arguments are only used to build the shared library
# not the executables that use the library.
lib_args = ['-DBUILDING_SIGNALS']

inc = include_directories('include')

shlib = shared_library('signals',
  'src/factory.cpp',
  'src/fd_wrapper.cpp',
  'src/fd_wrappers.cpp',
  'src/scoped_fd.cpp',
  'src/scoped_sigset.cpp',
  'src/siginfo.cpp',
  'src/signal_handler.cpp',
  'src/signalfd_handler.cpp',
  'src/sigset_util.cpp',
  'src/sigwait_handler.cpp',
  install : true,
  cpp_args : lib_args,
  gnu_symbol_visibility : 'hidden',
  include_directories : inc,
)

# Make this library usable as a Meson subproject.
signals_dep = declare_dependency(
  include_directories: inc,
  link_with : shlib)

# Make this library usable from the system's
# package manager.
install_headers(
  'include/signals/factory.h',
  'include/signals/fd_wrapper.h',
  'include/signals/fd_wrappers.h',
  'include/signals/scoped_fd.h',
  'include/signals/scoped_sigset.h',
  'include/signals/siginfo.h',
  'include/signals/signal_handler.h',
  'include/signals/signal_handler_callbacks.h',
  'include/signals/signalfd_handler.h',
  'include/signals/sigset_util.h',
  'include/signals/sigwait_handler.h',
  'include/signals/symbols.h',
  subdir: 'signals')

pkg_mod = import('pkgconfig')
pkg_mod.generate(
  name : 'signals',
  filebase : 'signals',
  description : 'Simple Unix signals wrapper library',
  libraries : shlib,
  version : '0.1',
)

subdir('test')
